﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CSGraphics
{
    public class ShaderProgram
    {
        public class ShaderProgramLinkException : Exception
        {
            public ShaderProgramLinkException(String error, String shaderProgramName)
                : base(String.Format("{0} in shader program {1}", (error != null && error != "") ? error : "No log returned", shaderProgramName))
            { }
        }

        public readonly String name;
        private ShaderProgramObject _object;
        private Dictionary<ShaderType, Shader> _boundShaders = new Dictionary<ShaderType, Shader>();
        private Dictionary<String, Uniform> _uniforms = new Dictionary<String, Uniform>();

        public ShaderProgram(String shaderProgramName)
        {
            this.name = shaderProgramName;
        }

        public void bindShader(Shader shader)
        {
            if (_object != null)
            {
                throw new InvalidOperationException("Cannot bind shader to shader program when it has already been linked");
            }

            _boundShaders.Add(shader.type, shader);
        }

        public void bind()
        {
            _upload();

            GLState.bindProgram(_object);
        }

        public Uniform getUniform(String name)
        {
            _upload();

            Uniform uniform;
            _uniforms.TryGetValue(name, out uniform);
            return uniform;
        }

        public T getUniform<T>(String name) where T : Uniform
        {
            _upload();

            Uniform uniform;
            _uniforms.TryGetValue(name, out uniform);
            if (uniform == null)
            {
                return null;
            }
            else
            {
                if (uniform.GetType() == typeof(T))
                {
                    return (T)uniform;
                }
                else
                {
                    return null;
                }
            }
        }

        public void uploadUniforms()
        {
            _upload();

            foreach (Uniform uniform in _uniforms.Values)
            {
                uniform.upload();
            }
        }

        private void _upload()
        {
            if (_object == null)
            {
                _object = new ShaderProgramObject();

                foreach (Shader shader in _boundShaders.Values)
                {
                    GL.AttachShader(_object.val, shader.getShaderObject().val);
                }

                GL.LinkProgram(_object.val);
                int status_code;
                GL.GetProgram(_object.val, GetProgramParameterName.LinkStatus, out status_code);
                if (status_code != 1)
                {
                    throw new ShaderProgramLinkException(GL.GetProgramInfoLog(_object.val), name);
                }

                _grabUniforms();
            }
        }

        private void _grabUniforms()
        {
            int numUniforms = 0;
            GL.GetProgram(_object.val, GetProgramParameterName.ActiveUniforms, out numUniforms);

            for (int i = 0; i < numUniforms; i++)
            {
                int arraySize = 0;
                int location = 0;
                ActiveUniformType type;
                String name = GL.GetActiveUniform(_object.val, i, out arraySize, out type);
                location = GL.GetUniformLocation(_object.val, name);

                Uniform uniform = _getUniform(name, location, type, arraySize);
                if (uniform != null)
                {
                    _uniforms.Add(name, uniform);
                }
            }
        }

        private Uniform _getUniform(String name, int location, ActiveUniformType type, int arraySize)
        {
            switch (type)
            {
                case ActiveUniformType.Float:
                    return new FloatUniform(this, name, location, arraySize, 1);
                case ActiveUniformType.FloatVec2:
                    return new FloatUniform(this, name, location, arraySize, 2);
                case ActiveUniformType.FloatVec3:
                    return new FloatUniform(this, name, location, arraySize, 3);
                case ActiveUniformType.FloatVec4:
                    return new FloatUniform(this, name, location, arraySize, 4);
                case ActiveUniformType.Int:
                    return new IntUniform(this, name, location, arraySize, 1);
                case ActiveUniformType.IntVec2:
                    return new IntUniform(this, name, location, arraySize, 2);
                case ActiveUniformType.IntVec3:
                    return new IntUniform(this, name, location, arraySize, 3);
                case ActiveUniformType.IntVec4:
                    return new IntUniform(this, name, location, arraySize, 4);
                case ActiveUniformType.UnsignedInt:
                    return new UintUniform(this, name, location, arraySize, 1);
                case ActiveUniformType.UnsignedIntVec2:
                    return new UintUniform(this, name, location, arraySize, 2);
                case ActiveUniformType.UnsignedIntVec3:
                    return new UintUniform(this, name, location, arraySize, 3);
                case ActiveUniformType.UnsignedIntVec4:
                    return new UintUniform(this, name, location, arraySize, 4);
                case ActiveUniformType.Bool:
                    return new BoolUniform(this, name, location, arraySize, 1);
                case ActiveUniformType.BoolVec2:
                    return new BoolUniform(this, name, location, arraySize, 2);
                case ActiveUniformType.BoolVec3:
                    return new BoolUniform(this, name, location, arraySize, 3);
                case ActiveUniformType.BoolVec4:
                    return new BoolUniform(this, name, location, arraySize, 4);
                case ActiveUniformType.FloatMat4:
                    return new Matrix4Uniform(this, name, location, arraySize);
                case ActiveUniformType.FloatMat3:
                    return new Matrix3Uniform(this, name, location, arraySize);
                case ActiveUniformType.Sampler1D:
                case ActiveUniformType.Sampler1DArray:
                case ActiveUniformType.Sampler1DArrayShadow:
                case ActiveUniformType.Sampler1DShadow:
                case ActiveUniformType.Sampler2D:
                case ActiveUniformType.Sampler2DArray:
                case ActiveUniformType.Sampler2DArrayShadow:
                case ActiveUniformType.Sampler2DShadow:
                case ActiveUniformType.Sampler3D:
                case ActiveUniformType.SamplerCube:
                case ActiveUniformType.SamplerCubeMapArray:
                case ActiveUniformType.SamplerCubeMapArrayShadow:
                case ActiveUniformType.SamplerCubeShadow:
                    return new SamplerUniform(this, name, location, arraySize);
            }
            return null;
        }
    }
}
