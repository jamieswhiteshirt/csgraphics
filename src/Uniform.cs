﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CSGraphics
{
    /// <summary>
    /// An abstract OpenGL uniform, a value bound to a shader program. Different types of uniform objects are assigned to different types of uniform values.
    /// </summary>
    public abstract class Uniform
    {
        public class InvalidUniformValueException : Exception
        {
            public InvalidUniformValueException(Uniform uniform, Object value)
                : base(String.Format("Invalid value {0} for uniform {1}", uniform.ToString(), value.ToString()))
            { }
        }

        public class UniformIndexOutOfBoundsException : Exception
        {
            public UniformIndexOutOfBoundsException(Uniform uniform, int index)
                : base(String.Format("Index {0} is out of bounds in uniform {1}", index, uniform.ToString()))
            { }
        }

        /// <summary>
        /// The program this uniform is bound to.
        /// </summary>
        public readonly ShaderProgram program;
        /// <summary>
        /// The name of this uniform.
        /// </summary>
        public readonly String name;
        /// <summary>
        /// The size of the array this uniform contains.
        /// </summary>
        public readonly int arraySize;
        protected int _location;
        private bool _modified;

        public Uniform(ShaderProgram program, String name, int location, int arraySize)
        {
            this.program = program;
            this.name = name;
            this._location = location;
            this.arraySize = arraySize;
            this._modified = true;
        }

        public override string ToString()
        {
            return String.Format("{0} of shader program {1}, array size {3}", name, program.name, _location, arraySize);
        }

        protected void _onEdit(int index)
        {
            _modified = true;
            if (index < 0 || index >= arraySize)
            {
                throw new UniformIndexOutOfBoundsException(this, index);
            }
        }

        /// <summary>
        /// Upload the data bound to this uniform.
        /// </summary>
        public void upload()
        {
            if (_modified)
            {
                _upload();
                _modified = false;
            }
        }

        protected abstract void _upload();
    }

    /// <summary>
    /// An abstract uniform for types that are stored as N-component vectors, but are not necessarily known as that type.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class VecBaseUniform<T> : Uniform where T : struct
    {
        public readonly int vecSize;
        protected T[] _values;

        public VecBaseUniform(ShaderProgram program, String name, int location, int arraySize, int vecSize)
            : base(program, name, location, arraySize)
        {
            this.vecSize = vecSize;
            this._values = new T[arraySize * vecSize];
        }
    }

    /// <summary>
    /// An abstract uniform for types that are stored as N-component vectors.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class VecUniform<T> : VecBaseUniform<T> where T : struct
    {
        public VecUniform(ShaderProgram program, String name, int location, int arraySize, int vecSize)
            : base(program, name, location, arraySize, vecSize)
        { }

        /// <summary>
        /// Set the 1-component value.
        /// </summary>
        /// <param name="val0">Value for component 0.</param>
        /// <param name="index">For arrays, the index.</param>
        public void set1(T val0, int index = 0)
        {
            _onEdit(index);
            _values[vecSize * index + 0] = val0;
        }

        /// <summary>
        /// Set the 2-component value.
        /// </summary>
        /// <param name="val0">Value for component 0.</param>
        /// <param name="val1">Value for component 1.</param>
        /// <param name="index">For arrays, the index.</param>
        public void set2(T val0, T val1, int index = 0)
        {
            _onEdit(index);
            _values[vecSize * index + 0] = val0;
            _values[vecSize * index + 1] = val1;
        }

        /// <summary>
        /// Set the 3-component value.
        /// </summary>
        /// <param name="val0">Value for component 0.</param>
        /// <param name="val1">Value for component 1.</param>
        /// <param name="val2">Value for component 2.</param>
        /// <param name="index">For arrays, the index.</param>
        public void set3(T val0, T val1, T val2, int index = 0)
        {
            _onEdit(index);
            _values[vecSize * index + 0] = val0;
            _values[vecSize * index + 1] = val1;
            _values[vecSize * index + 2] = val2;
        }

        /// <summary>
        /// Set the 4-component value.
        /// </summary>
        /// <param name="val0">Value for component 0.</param>
        /// <param name="val1">Value for component 1.</param>
        /// <param name="val2">Value for component 2.</param>
        /// <param name="val3">Value for component 3.</param>
        /// <param name="index">For arrays, the index.</param>
        public void set4(T val0, T val1, T val2, T val3, int index = 0)
        {
            _onEdit(index);
            _values[vecSize * index + 0] = val0;
            _values[vecSize * index + 1] = val1;
            _values[vecSize * index + 2] = val2;
            _values[vecSize * index + 3] = val3;
        }
    }

    /// <summary>
    /// A uniform type for N-component float vectors.
    /// </summary>
    public class FloatUniform : VecUniform<float>
    {
        public FloatUniform(ShaderProgram program, String name, int location, int arraySize, int vecSize)
            : base(program, name, location, arraySize, vecSize)
        { }

        protected override void _upload()
        {
            switch (vecSize)
            {
                case 1:
                    GL.Uniform1(_location, arraySize, _values);
                    break;
                case 2:
                    GL.Uniform2(_location, arraySize, _values);
                    break;
                case 3:
                    GL.Uniform3(_location, arraySize, _values);
                    break;
                case 4:
                    GL.Uniform4(_location, arraySize, _values);
                    break;
            }
        }
    }

    /// <summary>
    /// A uniform type for N-component int vectors.
    /// </summary>
    public class IntUniform : VecUniform<int>
    {
        public IntUniform(ShaderProgram program, String name, int location, int arraySize, int vecSize)
            : base(program, name, location, arraySize, vecSize)
        { }

        protected override void _upload()
        {
            switch (vecSize)
            {
                case 1:
                    GL.Uniform1(_location, arraySize, _values);
                    break;
                case 2:
                    GL.Uniform2(_location, arraySize, _values);
                    break;
                case 3:
                    GL.Uniform3(_location, arraySize, _values);
                    break;
                case 4:
                    GL.Uniform4(_location, arraySize, _values);
                    break;
            }
        }
    }

    /// <summary>
    /// A uniform type for N-component uint vectors.
    /// </summary>
    public class UintUniform : VecUniform<uint>
    {
        public UintUniform(ShaderProgram program, String name, int location, int arraySize, int vecSize)
            : base(program, name, location, arraySize, vecSize)
        { }

        protected override void _upload()
        {
            switch (vecSize)
            {
                case 1:
                    GL.Uniform1(_location, arraySize, _values);
                    break;
                case 2:
                    GL.Uniform2(_location, arraySize, _values);
                    break;
                case 3:
                    GL.Uniform3(_location, arraySize, _values);
                    break;
                case 4:
                    GL.Uniform4(_location, arraySize, _values);
                    break;
            }
        }
    }

    /// <summary>
    /// A uniform type for N-component bool vectors.
    /// </summary>
    public class BoolUniform : VecBaseUniform<int>
    {
        public BoolUniform(ShaderProgram program, String name, int location, int arraySize, int vecSize)
            : base(program, name, location, arraySize, vecSize)
        { }

        public void set1(bool val0, int index = 0)
        {
            _onEdit(index);
            _values[vecSize * index + 0] = val0 ? 1 : 0;
        }

        public void set2(bool val0, bool val1, int index = 0)
        {
            _onEdit(index);
            _values[vecSize * index + 0] = val0 ? 1 : 0;
            _values[vecSize * index + 1] = val1 ? 1 : 0;
        }

        public void set3(bool val0, bool val1, bool val2, int index = 0)
        {
            _onEdit(index);
            _values[vecSize * index + 0] = val0 ? 1 : 0;
            _values[vecSize * index + 1] = val1 ? 1 : 0;
            _values[vecSize * index + 2] = val2 ? 1 : 0;
        }

        public void set4(bool val0, bool val1, bool val2, bool val3, int index = 0)
        {
            _onEdit(index);
            _values[vecSize * index + 0] = val0 ? 1 : 0;
            _values[vecSize * index + 1] = val1 ? 1 : 0;
            _values[vecSize * index + 2] = val2 ? 1 : 0;
            _values[vecSize * index + 3] = val3 ? 1 : 0;
        }

        protected override void _upload()
        {
            GL.Uniform1(_location, arraySize, _values);
        }
    }

    /// <summary>
    /// A uniform type for 4x4 matrices.
    /// </summary>
    public class Matrix4Uniform : Uniform
    {
        private Matrix4[] _values;

        public Matrix4Uniform(ShaderProgram program, String name, int location, int arraySize)
            : base(program, name, location, arraySize)
        {
            _values = new Matrix4[arraySize];
        }

        /// <summary>
        /// Set the matrix value.
        /// </summary>
        /// <param name="val">The matrix.</param>
        /// <param name="index">For arrays, the index.</param>
        public void set(Matrix4 val, int index = 0)
        {
            _onEdit(index);
            _values[index] = val;
        }

        protected override void _upload()
        {
            float[] fValues = new float[arraySize * 16];
            for (int i = 0; i < arraySize; i++)
            {
                Matrix4 mat = _values[i];
                for (int y = 0; y < 4; y++)
                {
                    for (int x = 0; x < 4; x++)
                    {
                        fValues[x + y * 4 + i * 16] = mat[x, y];
                    }
                }
            }
            GL.UniformMatrix4(_location, arraySize, false, fValues);
        }
    }

    /// <summary>
    /// A uniform type for 3x3 matrices.
    /// </summary>
    public class Matrix3Uniform : Uniform
    {
        private Matrix3[] _values;

        public Matrix3Uniform(ShaderProgram program, String name, int location, int arraySize)
            : base(program, name, location, arraySize)
        {
            _values = new Matrix3[arraySize];
        }

        /// <summary>
        /// Set the matrix value.
        /// </summary>
        /// <param name="val">The matrix.</param>
        /// <param name="index">For arrays, the index.</param>
        public void set(Matrix3 val, int index = 0)
        {
            _onEdit(index);
            _values[index] = val;
        }

        protected override void _upload()
        {
            float[] fValues = new float[arraySize * 9];
            for (int i = 0; i < arraySize; i++)
            {
                Matrix3 mat = _values[i];
                for (int y = 0; y < 4; y++)
                {
                    for (int x = 0; x < 4; x++)
                    {
                        fValues[x + y * 3 + i * 9] = mat[x, y];
                    }
                }
            }
            GL.UniformMatrix3(_location, arraySize, false, fValues);
        }
    }

    /// <summary>
    /// A uniform type for samplers of any type.
    /// </summary>
    public class SamplerUniform : VecBaseUniform<int>
    {
        public SamplerUniform(ShaderProgram program, String name, int location, int arraySize)
            : base(program, name, location, arraySize, 1)
        { }

        /// <summary>
        /// Set the texture slot the sampler is bound to.
        /// </summary>
        /// <param name="val">The texture slot.</param>
        /// <param name="index">For arrays, the index.</param>
        public void set(int val, int index = 0)
        {
            _onEdit(index);
            _values[vecSize * index] = val;
        }

        protected override void _upload()
        {
            GL.Uniform1(_location, arraySize, _values);
        }
    }
}
