﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CSGraphics
{
    public abstract class Texture : TexDataProvider
    {
        public class BitmapFormatException : Exception
        {
            public BitmapFormatException(String message)
                : base(message)
            { }
        }

        public class InvalidDataException : Exception
        {
            public InvalidDataException(String message)
                : base(message)
            { }
        }

        private TextureMagFilter _magFilter;
        public TextureMagFilter magFilter
        {
            get { return _magFilter; }
            set
            {
                _magFilter = value;
                texImage[TextureParameterName.TextureMagFilter] = (int)value;
            }
        }

        private TextureMinFilter _minFilter;
        public TextureMinFilter minFilter
        {
            get { return _minFilter; }
            set
            {
                _minFilter = value;
                texImage[TextureParameterName.TextureMinFilter] = (int)value;
            }
        }

        private TextureWrapMode _wrap;
        public TextureWrapMode wrap
        {
            get { return _wrap; }
            set
            {
                _wrap = value;
                texImage[TextureParameterName.TextureWrapS] = (int)value;
                texImage[TextureParameterName.TextureWrapT] = (int)value;
            }
        }

        protected byte[] _data;

        public abstract TexImage texImage
        {
            get;
        }

        public void setTextureParameters(TextureMagFilter magFilter, TextureMinFilter minFilter, TextureWrapMode wrap)
        {
            this.magFilter = magFilter;
            this.minFilter = minFilter;
            this.wrap = wrap;
        }

        public Texture()
        {}

        protected static void _assertPixelFormat(System.Drawing.Imaging.PixelFormat pixelFormat)
        {
            if (pixelFormat != System.Drawing.Imaging.PixelFormat.Format24bppRgb && pixelFormat != System.Drawing.Imaging.PixelFormat.Format32bppArgb)
            {
                throw new BitmapFormatException(String.Format("Invalid bitmap pixel format {0} for texture", pixelFormat.ToString()));
            }
        }

        protected static Dictionary<System.Drawing.Imaging.PixelFormat, PixelInternalFormat> _internalFormats = new Dictionary<System.Drawing.Imaging.PixelFormat, PixelInternalFormat>();
        protected static Dictionary<System.Drawing.Imaging.PixelFormat, PixelFormat> _pixelFormats = new Dictionary<System.Drawing.Imaging.PixelFormat, PixelFormat>();
        protected static Dictionary<System.Drawing.Imaging.PixelFormat, int> _bytesPerPixel = new Dictionary<System.Drawing.Imaging.PixelFormat, int>();

        static Texture()
        {
            _internalFormats.Add(System.Drawing.Imaging.PixelFormat.Format24bppRgb, PixelInternalFormat.Rgb);
            _pixelFormats.Add(System.Drawing.Imaging.PixelFormat.Format24bppRgb, PixelFormat.Bgr);
            _bytesPerPixel.Add(System.Drawing.Imaging.PixelFormat.Format24bppRgb, 3);
            _internalFormats.Add(System.Drawing.Imaging.PixelFormat.Format32bppArgb, PixelInternalFormat.Rgba);
            _pixelFormats.Add(System.Drawing.Imaging.PixelFormat.Format32bppArgb, PixelFormat.Bgra);
            _bytesPerPixel.Add(System.Drawing.Imaging.PixelFormat.Format32bppArgb, 4);
        }

        public void bind()
        {
            texImage.bind();
        }

        byte[] TexDataProvider.getData(TexImage texImage)
        {
            return _data;
        }
    }

    public class Texture1D : Texture
    {
        private TexImage1D _texImage;

        public override TexImage texImage
        {
            get { return _texImage; }
        }

        public Texture1D(Bitmap bitmap, TextureTarget target, TextureMagFilter magFilter, TextureMinFilter minFilter, TextureWrapMode wrap)
            : base()
        {
            _assertPixelFormat(bitmap.PixelFormat);

            this._texImage = new TexImage1D(this, target, _internalFormats[bitmap.PixelFormat], _pixelFormats[bitmap.PixelFormat], PixelType.UnsignedByte, bitmap.Width);
            setTextureParameters(magFilter, minFilter, wrap);

            System.Drawing.Imaging.BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);
            this._data = new byte[bitmap.Width * _bytesPerPixel[bitmap.PixelFormat]];
            System.Runtime.InteropServices.Marshal.Copy(bitmapData.Scan0, _data, 0, _data.Length);

            bitmap.UnlockBits(bitmapData);
        }

        public Texture1D(byte[] data, System.Drawing.Imaging.PixelFormat pixelFormat, int width, TextureTarget target, TextureMagFilter magFilter, TextureMinFilter minFilter, TextureWrapMode wrap)
            : base()
        {
            _assertPixelFormat(pixelFormat);

            this._texImage = new TexImage1D(this, target, _internalFormats[pixelFormat], _pixelFormats[pixelFormat], PixelType.UnsignedByte, width);
            setTextureParameters(magFilter, minFilter, wrap);

            if (data.Length != width * _bytesPerPixel[pixelFormat])
            {
                throw new InvalidDataException(String.Format("Invalid data size {0}, expected {1}", data.Length, width * _bytesPerPixel[pixelFormat]));
            }

            this._data = (byte[])data.Clone();
        }
    }

    public class Texture2D : Texture
    {
        private TexImage2D _texImage;

        public override TexImage texImage
        {
            get { return _texImage; }
        }

        public Texture2D(Bitmap bitmap, TextureTarget target, TextureMagFilter magFilter, TextureMinFilter minFilter, TextureWrapMode wrap)
            : base()
        {
            this._texImage = new TexImage2D(this, target, _internalFormats[bitmap.PixelFormat], _pixelFormats[bitmap.PixelFormat], PixelType.UnsignedByte, bitmap.Width, bitmap.Height);
            setTextureParameters(magFilter, minFilter, wrap);

            System.Drawing.Imaging.BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);
            int stride = bitmap.Width * _bytesPerPixel[bitmap.PixelFormat];
            this._data = new byte[bitmap.Height * stride];
            for(int i = 0; i < bitmap.Height; i++)
            {
                int j = bitmap.Height - i - 1;
                System.Runtime.InteropServices.Marshal.Copy(bitmapData.Scan0 + j * stride, _data, i * stride, stride);
            }

            bitmap.UnlockBits(bitmapData);
        }

        public Texture2D(byte[] data, System.Drawing.Imaging.PixelFormat pixelFormat, int width, int height, TextureTarget target, TextureMagFilter magFilter, TextureMinFilter minFilter, TextureWrapMode wrap)
            : base()
        {
            _assertPixelFormat(pixelFormat);

            this._texImage = new TexImage2D(this, target, _internalFormats[pixelFormat], _pixelFormats[pixelFormat], PixelType.UnsignedByte, width, height);
            setTextureParameters(magFilter, minFilter, wrap);

            if (data.Length != width * height * _bytesPerPixel[pixelFormat])
            {
                throw new InvalidDataException(String.Format("Invalid data size {0}, expected {1}", data.Length, width * height * _bytesPerPixel[pixelFormat]));
            }

            this._data = (byte[])data.Clone();
        }
    }
}
