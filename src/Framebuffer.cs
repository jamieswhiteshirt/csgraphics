﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CSGraphics
{
    public abstract class FramebufferBase
    {
        public abstract int width
        {
            get;
        }

        public abstract int height
        {
            get;
        }

        protected abstract FramebufferObject getFramebufferObject();

        public void use()
        {
            GLState.bindFramebuffer(getFramebufferObject());
            GL.Viewport(0, 0, width, height);
        }
    }

    public abstract class DefaultFramebuffer : FramebufferBase
    {
        protected override FramebufferObject getFramebufferObject()
        {
            return null;
        }
    }

    class Framebuffer : FramebufferBase
    {
        private FramebufferObject _object;
        private Dictionary<FramebufferAttachment, TexImage> _attachments;
        private int _width, _height;

        public override int width
        {
            get
            {
                return _width;
            }
        }

        public override int height
        {
            get
            {
                return _height;
            }
        }

        protected override FramebufferObject getFramebufferObject()
        {
            _upload();

            return _object;
        }

        public Framebuffer(int width, int height)
        {
            this._width = width;
            this._height = height;
            _attachments = new Dictionary<FramebufferAttachment, TexImage>();
        }

        private void _upload()
        {
            if (_object == null)
            {
                _object = new FramebufferObject();

                FramebufferObject previousFramebuffer = GLState.getFramebuffer();
                GLState.bindFramebuffer(_object);
                foreach (KeyValuePair<FramebufferAttachment, TexImage> entry in _attachments)
                {
                    GL.FramebufferTexture(FramebufferTarget.Framebuffer, entry.Key, entry.Value.getTextureObject().val, 0);
                }
                GLState.bindFramebuffer(previousFramebuffer);
            }
        }

        public bool isUploaded()
        {
            return _object != null;
        }

        public TexImage this[FramebufferAttachment attachment]
        {
            get
            {
                return _attachments[attachment];
            }
            set
            {
                _attachments[attachment] = value;
                if (isUploaded())
                {
                    GL.FramebufferTexture(FramebufferTarget.Framebuffer, attachment, value.getTextureObject().val, 0);
                }
            }
        }
    }
}
