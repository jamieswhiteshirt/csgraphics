﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using System.IO;

namespace CSGraphics
{
    /// <summary>
    /// An OpenGL shader. Supports the use of a simple templater.
    /// </summary>
    public abstract class Shader
    {
        public class ShaderCompileException : Exception
        {
            public ShaderCompileException(String error, String shaderName)
                : base(String.Format("{0} in shader {1}", (error != null && error != "") ? error : "No log returned", shaderName))
            { }
        }

        public readonly ShaderType type;
        public readonly String name;
        public readonly String source;
        private ShaderObject _object;

        /// <summary>
        /// An OpenGL shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader.</param>
        /// <param name="source">The source string of the shader.</param>
        /// <param name="parameters">A set of parameters to be used by the shader templater.</param>
        /// <param name="type">The type of shader.</param>
        protected Shader(String shaderName, String source, Dictionary<String, String> parameters, ShaderType type)
        {
            this.type = type;
            this.name = shaderName;
            this.source = ShaderTemplater.parseSource(shaderName, source, parameters);
        }

        /// <summary>
        /// An OpenGL shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader.</param>
        /// <param name="source">The source string of the shader.</param>
        /// <param name="type">The type of shader.</param>
        protected Shader(String shaderName, String source, ShaderType type)
            : this(shaderName, source, new Dictionary<String, String>(), type)
        { }

        /// <summary>
        /// An OpenGL shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader which will be used by the ShaderSourceLoader to get a source string.</param>
        /// <param name="parameters">A set of parameters to be used by the shader templater.</param>
        /// <param name="type">The type of shader.</param>
        protected Shader(String shaderName, Dictionary<String, String> parameters, ShaderType type)
        {
            this.type = type;
            this.name = shaderName;
            this.source = ShaderTemplater.parse(shaderName, parameters);
        }

        /// <summary>
        /// An OpenGL shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader which will be used by the ShaderSourceLoader to get a source string.</param>
        /// <param name="type">The type of shader.</param>
        protected Shader(String shaderName, ShaderType type)
            : this(shaderName, new Dictionary<String, String>(), type)
        { }

        private void _upload()
        {
            if (_object == null)
            {
                _object = new ShaderObject(type);

                GL.ShaderSource(_object.val, source);
                GL.CompileShader(_object.val);

                int status_code;
                GL.GetShader(_object.val, ShaderParameter.CompileStatus, out status_code);
                if (status_code != 1)
                {
                    throw new ShaderCompileException(GL.GetShaderInfoLog(_object.val), name);
                }
            }
        }

        /// <summary>
        /// Get the underlying ShaderObject this encapsulates.
        /// </summary>
        /// <returns></returns>
        public ShaderObject getShaderObject()
        {
            _upload();
            return _object;
        }
    }

    /// <summary>
    /// An OpenGL vertex shader. Supports the use of a simple templater.
    /// </summary>
    public class VertexShader : Shader
    {
        /// <summary>
        /// An OpenGL vertex shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader.</param>
        /// <param name="source">The source string of the shader.</param>
        /// <param name="parameters">A set of parameters to be used by the shader templater.</param>
        public VertexShader(String shaderName, String source, Dictionary<String, String> parameters)
            : base(shaderName, source, parameters, ShaderType.VertexShader)
        { }

        /// <summary>
        /// An OpenGL vertex shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader.</param>
        /// <param name="source">The source string of the shader.</param>
        public VertexShader(String shaderName, String source)
            : base(shaderName, source, ShaderType.VertexShader)
        { }

        /// <summary>
        /// An OpenGL vertex shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader which will be used by the ShaderSourceLoader to get a source string.</param>
        /// <param name="parameters">A set of parameters to be used by the shader templater.</param>
        public VertexShader(String shaderName, Dictionary<String, String> parameters)
            : base(shaderName, parameters, ShaderType.VertexShader)
        { }

        /// <summary>
        /// An OpenGL vertex shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader which will be used by the ShaderSourceLoader to get a source string.</param>
        public VertexShader(String shaderName)
            : base(shaderName, ShaderType.VertexShader)
        { }
    }

    /// <summary>
    /// An OpenGL fragment shader. Supports the use of a simple templater.
    /// </summary>
    public class FragmentShader : Shader
    {
        /// <summary>
        /// An OpenGL fragment shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader.</param>
        /// <param name="source">The source string of the shader.</param>
        /// <param name="parameters">A set of parameters to be used by the shader templater.</param>
        public FragmentShader(String shaderName, String source, Dictionary<String, String> parameters)
            : base(shaderName, source, parameters, ShaderType.FragmentShader)
        { }

        /// <summary>
        /// An OpenGL fragment shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader.</param>
        /// <param name="source">The source string of the shader.</param>
        public FragmentShader(String shaderName, String source)
            : base(shaderName, source, ShaderType.FragmentShader)
        { }

        /// <summary>
        /// An OpenGL fragment shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader which will be used by the ShaderSourceLoader to get a source string.</param>
        /// <param name="parameters">A set of parameters to be used by the shader templater.</param>
        public FragmentShader(String shaderName, Dictionary<String, String> parameters)
            : base(shaderName, parameters, ShaderType.FragmentShader)
        { }

        /// <summary>
        /// An OpenGL fragment shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader which will be used by the ShaderSourceLoader to get a source string.</param>
        public FragmentShader(String shaderName)
            : base(shaderName, ShaderType.FragmentShader)
        { }
    }

    /// <summary>
    /// An OpenGL geometry shader. Supports the use of a simple templater.
    /// </summary>
    public class GeometryShader : Shader
    {
        /// <summary>
        /// An OpenGL geometry shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader.</param>
        /// <param name="source">The source string of the shader.</param>
        /// <param name="parameters">A set of parameters to be used by the shader templater.</param>
        public GeometryShader(String shaderName, String source, Dictionary<String, String> parameters)
            : base(shaderName, source, parameters, ShaderType.GeometryShader)
        { }

        /// <summary>
        /// An OpenGL geometry shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader.</param>
        /// <param name="source">The source string of the shader.</param>
        public GeometryShader(String shaderName, String source)
            : base(shaderName, source, ShaderType.GeometryShader)
        { }

        /// <summary>
        /// An OpenGL geometry shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader which will be used by the ShaderSourceLoader to get a source string.</param>
        /// <param name="parameters">A set of parameters to be used by the shader templater.</param>
        public GeometryShader(String shaderName, Dictionary<String, String> parameters)
            : base(shaderName, parameters, ShaderType.GeometryShader)
        { }

        /// <summary>
        /// An OpenGL geometry shader. Supports the use of a simple templater.
        /// </summary>
        /// <param name="shaderName">The name of the shader which will be used by the ShaderSourceLoader to get a source string.</param>
        public GeometryShader(String shaderName)
            : base(shaderName, ShaderType.GeometryShader)
        { }
    }
}
