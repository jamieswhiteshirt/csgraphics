﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CSGraphics
{
    /// <summary>
    /// An interface for OpenGL objects that have special construction and destruction needs.
    /// </summary>
    public interface GLObject
    {
        /// <summary>
        /// Free the underlying OpenGL resource.
        /// </summary>
        void destroy();
    }

    /// <summary>
    /// Wrapper for OpenGL buffer objects.
    /// </summary>
    public class BufferObject : GLObject
    {
        private int _val;
        public int val
        {
            get { return _val; }
        }

        /// <summary>
        /// Wrapper for OpenGL buffer objects.
        /// </summary>
        public BufferObject()
        {
            _val = GL.GenBuffer();
        }

        void GLObject.destroy()
        {
            GL.DeleteBuffer(_val);
            _val = 0;
        }
    }

    /// <summary>
    /// Wrapper for OpenGL shader objects.
    /// </summary>
    public class ShaderObject : GLObject
    {
        private int _val;
        public int val
        {
            get { return _val; }
        }

        /// <summary>
        /// Wrapper for OpenGL shader objects.
        /// </summary>
        public ShaderObject(ShaderType type)
        {
            _val = GL.CreateShader(type);
        }

        void GLObject.destroy()
        {
            GL.DeleteShader(_val);
            _val = 0;
        }
    }

    /// <summary>
    /// Wrapper for OpenGL shader program objects.
    /// </summary>
    public class ShaderProgramObject : GLObject
    {
        private int _val;
        public int val
        {
            get { return _val; }
        }

        /// <summary>
        /// Wrapper for OpenGL shader program objects.
        /// </summary>
        public ShaderProgramObject()
        {
            _val = GL.CreateProgram();
        }

        void GLObject.destroy()
        {
            GL.DeleteProgram(_val);
            _val = 0;
        }
    }

    /// <summary>
    /// Wrapper for OpenGL vertex array objects.
    /// </summary>
    public class VertexArrayObject : GLObject
    {
        private int _val;
        public int val
        {
            get { return _val; }
        }

        /// <summary>
        /// Wrapper for OpenGL vertex array objects.
        /// </summary>
        public VertexArrayObject()
        {
            _val = GL.GenVertexArray();
        }

        void GLObject.destroy()
        {
            GL.DeleteVertexArray(_val);
            _val = 0;
        }
    }

    public class TextureObject : GLObject
    {
        private int _val;
        public int val
        {
            get { return _val; }
        }

        /// <summary>
        /// Wrapper for OpenGL vertex array objects.
        /// </summary>
        public TextureObject()
        {
            _val = GL.GenTexture();
        }

        void GLObject.destroy()
        {
            GL.DeleteTexture(_val);
            _val = 0;
        }
    }

    public class FramebufferObject : GLObject
    {
        private int _val;
        public int val
        {
            get { return _val; }
        }

        /// <summary>
        /// Wrapper for OpenGL vertex array objects.
        /// </summary>
        public FramebufferObject()
        {
            _val = GL.GenFramebuffer();
        }

        void GLObject.destroy()
        {
            GL.DeleteFramebuffer(_val);
            _val = 0;
        }
    }
}
