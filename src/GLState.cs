﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;

namespace CSGraphics
{
    /// <summary>
    /// An utility that stores OpenGL state parameters.
    /// </summary>
    public class GLState
    {
        private static GLState _globalState = new GLState();

        private ShaderProgramObject _shaderProgram;
        private Dictionary<BufferTarget, BufferObject> _buffers;
        private VertexArrayObject _vertexArray;
        private Dictionary<TextureTarget, TextureObject> _textures;
        private FramebufferObject _frameBuffer;

        private GLState()
        {
            this._shaderProgram = null;
            this._buffers = new Dictionary<BufferTarget, BufferObject>();
            this._vertexArray = null;
            this._textures = new Dictionary<TextureTarget, TextureObject>();
        }

        private GLState(GLState other)
        {
            this._shaderProgram = other._shaderProgram;
            this._buffers = new Dictionary<BufferTarget, BufferObject>(other._buffers);
            this._vertexArray = other._vertexArray;
            this._textures = new Dictionary<TextureTarget, TextureObject>(other._textures);
        }

        public static GLState capture()
        {
            return new GLState(_globalState);
        }

        public static void bindProgram(ShaderProgramObject obj)
        {
            _globalState._shaderProgram = obj;
            GL.UseProgram(obj != null ? obj.val : 0);
        }

        public static void unbindProgram()
        {
            bindProgram(null);
        }

        public static ShaderProgramObject getProgram()
        {
            return _globalState._shaderProgram;
        }

        public static void bindBuffer(BufferTarget target, BufferObject obj)
        {
            if (obj != null)
            {
                _globalState._buffers[target] = obj;
                GL.BindBuffer(target, obj.val);
            }
            else
            {
                _globalState._buffers.Remove(target);
                GL.BindBuffer(target, 0);
            }
        }

        public static void unbindBuffer(BufferTarget target)
        {
            bindBuffer(target, null);
        }

        public BufferObject getBuffer(BufferTarget target)
        {
            BufferObject res = null;
            _globalState._buffers.TryGetValue(target, out res);
            return res;
        }

        public static void bindVertexArray(VertexArrayObject obj)
        {
            _globalState._vertexArray = obj;
            GL.BindVertexArray(obj != null ? obj.val : 0);
        }

        public static void unbindVertexArray()
        {
            bindVertexArray(null);
        }

        public static VertexArrayObject getVertexArray()
        {
            return _globalState._vertexArray;
        }

        public static void bindTexture(TextureTarget target, TextureObject obj)
        {
            if (obj != null)
            {
                _globalState._textures[target] = obj;
                GL.BindTexture(target, obj.val);
            }
            else
            {
                _globalState._textures.Remove(target);
                GL.BindTexture(target, 0);
            }
        }

        public static void unbindTexture(TextureTarget target)
        {
            bindTexture(target, null);
        }

        public static TextureObject getTexture(TextureTarget target)
        {
            TextureObject res = null;
            _globalState._textures.TryGetValue(target, out res);
            return res;
        }

        public static void bindFramebuffer(FramebufferObject obj)
        {
            _globalState._frameBuffer = obj;
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, obj != null ? obj.val : 0);
        }

        public static void unbindFramebuffer()
        {
            bindFramebuffer(null);
        }

        public static FramebufferObject getFramebuffer()
        {
            return _globalState._frameBuffer;
        }
    }
}
