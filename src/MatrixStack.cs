﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace CSGraphics
{
    public abstract class MultiplicationStack<T>
    {
        private List<T> _stack;

        public MultiplicationStack()
        {
            _stack = new List<T>();
            _stack.Add(_initialValue());
        }

        public T get()
        {
            return _stack[_stack.Count - 1];
        }

        public void push(T value)
        {
            _stack.Add(_multiply(_stack[_stack.Count - 1], value));
        }

        public void pop()
        {
            _stack.RemoveAt(_stack.Count - 1);
        }

        public void multiply(T value)
        {
            _stack[_stack.Count - 1] = _multiply(_stack[_stack.Count - 1], value);
        }

        public void clear()
        {
            _stack.Clear();
            _stack.Add(_initialValue());
        }

        protected abstract T _multiply(T m0, T m1);

        protected abstract T _initialValue();
    }

    public class Matrix3Stack : MultiplicationStack<Matrix3>
    {
        protected override Matrix3 _multiply(Matrix3 m0, Matrix3 m1)
        {
            return m0 * m1;
        }

        protected override Matrix3 _initialValue()
        {
            return Matrix3.Identity;
        }
    }

    public class Matrix4Stack : MultiplicationStack<Matrix4>
    {
        protected override Matrix4 _multiply(Matrix4 m0, Matrix4 m1)
        {
            return m0 * m1;
        }

        protected override Matrix4 _initialValue()
        {
            return Matrix4.Identity;
        }
    }
}
