﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CSGraphics
{
    /// <summary>
    /// An abstract wrapper for OpenGL vertex attribute data.
    /// </summary>
    public abstract class AttributeData
    {
        protected BufferUsageHint _usageHint;
        protected BufferObject _object;

        /// <summary>
        /// An abstract wrapper for OpenGL vertex attribute data.
        /// </summary>
        /// <param name="usageHint">A hint to OpenGL on the usage of this data that will affect how it is stored in memory.</param>
        protected AttributeData(BufferUsageHint usageHint)
        {
            _usageHint = usageHint;
        }

        /// <summary>
        /// Get the underlying BufferObject this encapsulates.
        /// </summary>
        /// <returns></returns>
        public BufferObject getObject()
        {
            _upload();
            return _object;
        }

        protected abstract void _upload();

        /// <summary>
        /// The actual size of the data.
        /// </summary>
        public abstract int size
        {
            get;
        }

        /// <summary>
        /// The type of data stored.
        /// </summary>
        public abstract Type type
        {
            get;
        }
    }

    /// <summary>
    /// An AttributeData implementation that has a static size. The whole buffer does not have to be filled.
    /// </summary>
    /// <typeparam name="T">The type of data that is stored in the buffer.</typeparam>
    public class StaticAttributeData<T> : AttributeData where T : struct
    {
        private readonly T[] _data;
        private int _size = 0;

        public override int size
        {
            get { return _size; }
        }

        public override Type type
        {
            get { return typeof(T); }
        }

        /// <summary>
        /// An AttributeData implementation that has a static size. The whole buffer does not have to be filled.
        /// </summary>
        /// <param name="usageHint">A hint to OpenGL on the usage of this data that will affect how it is stored in memory.</param>
        /// <param name="allocationSize">The amount of data that will be allocated.</param>
        public StaticAttributeData(BufferUsageHint usageHint, int allocationSize)
            : base(usageHint)
        {
            _data = new T[allocationSize];
        }

        protected override void _upload()
        {
            if (_object == null)
            {
                _object = new BufferObject();

                GLState.bindBuffer(BufferTarget.ArrayBuffer, _object);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(_size * System.Runtime.InteropServices.Marshal.SizeOf(typeof(T))), _data, _usageHint);
            }
        }

        public int add(T val)
        {
            _data[size] = val;
            return _size++;
        }

        public T this[int i]
        {
            get { return _data[i]; }
            set { _data[i] = value; }
        }
    }

    /// <summary>
    /// An AttributeData implementation that has a dynamic size.
    /// </summary>
    /// <typeparam name="T">The type of data that is stored in the buffer.</typeparam>
    public class DynamicAttributeData<T> : AttributeData where T : struct
    {
        private readonly List<T> _data;

        public override int size
        {
            get { return _data.Count(); }
        }

        public override Type type
        {
            get { return typeof(T); }
        }

        /// <summary>
        /// An AttributeData implementation that has a dynamic size.
        /// </summary>
        /// <param name="usageHint">A hint to OpenGL on the usage of this data that will affect how it is stored in memory.</param>
        /// <param name="allocationSize">The amount of data that will be pre-allocated.</param>
        public DynamicAttributeData(BufferUsageHint usageHint, int allocationSize)
            : base(usageHint)
        {
            _data = new List<T>(allocationSize);
        }

        protected override void _upload()
        {
            if (_object == null)
            {
                _object = new BufferObject();

                GLState.bindBuffer(BufferTarget.ArrayBuffer, _object);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(_data.Count * System.Runtime.InteropServices.Marshal.SizeOf(typeof(T))), _data.ToArray(), _usageHint);
            }
        }

        public int add(T val)
        {
            _data.Add(val);
            return _data.Count();
        }

        public T this[int i]
        {
            get { return _data[i]; }
            set { _data[i] = value; }
        }
    }

    /// <summary>
    /// A many-to-many relationship between AttributeData and VertexStream. An AttributePointer provides information on how OpenGL will read the AttributeData.
    /// </summary>
    public struct AttributePointer
    {
        public readonly int index;
        public readonly int size;
        public readonly VertexAttribPointerType type;
        public readonly bool normalized;
        public readonly int stride;
        public readonly int offset;
        public readonly AttributeData data;

        /// <summary>
        /// A many-to-many relationship between AttributeData and VertexStream. An AttributePointer provides information on how OpenGL will read the AttributeData.
        /// </summary>
        /// <param name="index">The index the pointer is bound to. Must be unique. The index is equivalent to the location: "layout(location = 0) in vec4 in_Vertex;"</param>
        /// <param name="size">The number of components.</param>
        /// <param name="type">The data type.</param>
        /// <param name="normalized">Should the input be normalized? Only works on floating point data.</param>
        /// <param name="stride">The number of bytes between consecutive attributes plus the size of the attribute. If the attributes are tightly packed, let this be 0.</param>
        /// <param name="offset">The byte offset to the beginning of the data.</param>
        /// <param name="data">The data this pointer will be bound to.</param>
        public AttributePointer(int index, int size, VertexAttribPointerType type, bool normalized, int stride, int offset, AttributeData data)
        {
            this.index = index;
            this.size = size;
            this.type = type;
            this.normalized = normalized;
            this.stride = stride;
            this.offset = offset;
            this.data = data;
        }
    }

    public class VertexStream
    {
        public class AttributeBindingException : Exception
        {
            public AttributeBindingException(String message)
                : base(message)
            { }
        }

        public class IndexArrayBindingException : Exception
        {
            public IndexArrayBindingException(String message)
                : base(message)
            { }
        }

        private struct TypeIndexBinding
        {
            public readonly int byteSize;
            public readonly DrawElementsType elementsType;

            public TypeIndexBinding(int byteSize, DrawElementsType elementsType)
            {
                this.byteSize = byteSize;
                this.elementsType = elementsType;
            }
        }

        private VertexArrayObject _object;
        private Dictionary<int, AttributePointer> _attributePointers = new Dictionary<int,AttributePointer>();
        private AttributeData _indices;
        private static Dictionary<Type, TypeIndexBinding> _typeIndexBindings = new Dictionary<Type, TypeIndexBinding>();

        static VertexStream()
        {
            _typeIndexBindings.Add(typeof(sbyte), new TypeIndexBinding(1, DrawElementsType.UnsignedByte));
            _typeIndexBindings.Add(typeof(byte), new TypeIndexBinding(1, DrawElementsType.UnsignedByte));
            _typeIndexBindings.Add(typeof(short), new TypeIndexBinding(2, DrawElementsType.UnsignedShort));
            _typeIndexBindings.Add(typeof(ushort), new TypeIndexBinding(2, DrawElementsType.UnsignedShort));
            _typeIndexBindings.Add(typeof(int), new TypeIndexBinding(4, DrawElementsType.UnsignedInt));
            _typeIndexBindings.Add(typeof(uint), new TypeIndexBinding(4, DrawElementsType.UnsignedInt));
        }

        /// <summary>
        /// To use indexed rendering, you can bind AttributeData to the indexArray. Must be of type sbyte, byte, short, ushort, int or uint.
        /// </summary>
        public AttributeData indexArray
        {
            get { return _indices; }
            set {
                if (value == null || _typeIndexBindings.ContainsKey(value.type))
                {
                    _indices = value;
                }
                else
                {
                    throw new IndexArrayBindingException("Invalid type for index array binding. Type must be sbyte, byte, short, ushort, int or uint.");
                }
            }
        }

        private void _upload()
        {
            if (_object == null)
            {
                _object = new VertexArrayObject();
                GLState.bindVertexArray(_object);

                if (_indices != null)
                {
                    GLState.bindBuffer(BufferTarget.ElementArrayBuffer, _indices.getObject());
                }
                else
                {
                    GLState.unbindBuffer(BufferTarget.ElementArrayBuffer);
                }

                foreach (AttributePointer attributePointer in _attributePointers.Values)
                {
                    GLState.bindBuffer(BufferTarget.ArrayBuffer, attributePointer.data.getObject());
                    GL.EnableVertexAttribArray(attributePointer.index);
                    GL.VertexAttribPointer(attributePointer.index, attributePointer.size, attributePointer.type, attributePointer.normalized, attributePointer.stride, attributePointer.offset);
                }
            }
        }

        /// <summary>
        /// Add an AttributePointer to bind AttributeData to the VertexStream.
        /// </summary>
        /// <param name="attributePointer"></param>
        public void addAttributePointer(AttributePointer attributePointer)
        {
            if (!_attributePointers.ContainsKey(attributePointer.index))
            {
                _attributePointers.Add(attributePointer.index, attributePointer);
            }
            else
            {
                throw new AttributeBindingException(String.Format("Attribute already bound to index {0}", attributePointer.index));
            }
        }

        /// <summary>
        /// Render the content of the VertexStream. Its behaviour will change if an index buffer is bound.
        /// </summary>
        /// <param name="count">The number of elements to render. If 0, it will render as many as possible.</param>
        /// <param name="first">The index of the first element to render.</param>
        /// <param name="mode">The mode at which elements are drawn.</param>
        public void render(int count = 0, int first = 0, PrimitiveType primitiveType = PrimitiveType.Triangles)
        {
            _upload();

            GLState.bindVertexArray(_object);

            if (_indices != null)
            {
                if (count == 0)
                {
                    count = _indices.size - first;
                }
                if (count > 0)
                {
                    TypeIndexBinding typeIndexBinding = _typeIndexBindings[_indices.type];
                    GL.DrawElements(primitiveType, count, typeIndexBinding.elementsType, typeIndexBinding.byteSize * first);
                }
            }
            else
            {
                if (count == 0)
                {
                    foreach (AttributePointer attributePointer in _attributePointers.Values)
                    {
                        if (count == 0 || count > attributePointer.data.size)
                        {
                            count = attributePointer.data.size;
                        }
                    }
                }
                if (count > 0)
                {
                    GL.DrawArrays(primitiveType, first, count);
                }
            }
        }
    }
}
