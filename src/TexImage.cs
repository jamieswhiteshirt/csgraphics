﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CSGraphics
{
    public interface TexDataProvider
    {
        byte[] getData(TexImage texImage);
    }

    public abstract class TexImage
    {
        protected TextureObject _object;
        protected Dictionary<TextureParameterName, int> _intParameters = new Dictionary<TextureParameterName,int>();
        public readonly TexDataProvider dataProvider;
        public readonly TextureTarget target;
        public readonly PixelInternalFormat internalFormat;
        public readonly PixelFormat format;
        public readonly PixelType type;

        public TexImage(TextureTarget target, PixelInternalFormat internalFormat, PixelFormat format, PixelType type)
        {
            this.dataProvider = null;
            this.target = target;
            this.internalFormat = internalFormat;
            this.format = format;
            this.type = type;
        }

        public TexImage(TexDataProvider dataProvider, TextureTarget target, PixelInternalFormat internalFormat, PixelFormat format, PixelType type)
        {
            this.dataProvider = dataProvider;
            this.target = target;
            this.internalFormat = internalFormat;
            this.format = format;
            this.type = type;
        }

        private void _upload()
        {
            if (_object == null)
            {
                _object = new TextureObject();

                TextureObject previousTexture = GLState.getTexture(target);
                GLState.bindTexture(target, _object);
                _setParameters();
                _uploadData();
                GLState.bindTexture(target, previousTexture);
            }
        }

        private void _setParameters()
        {
            foreach(KeyValuePair<TextureParameterName, int> entry in _intParameters)
            {
                GL.TexParameter(target, entry.Key, entry.Value);
            }
        }

        protected void _onResize()
        {
            if (_object != null)
            {
                _uploadData();
            }
        }

        public bool isUploaded()
        {
            return _object != null;
        }

        protected abstract void _uploadData();

        public void bind()
        {
            _upload();
            GLState.bindTexture(target, _object);
        }

        public int this[TextureParameterName parameterName]
        {
            get
            {
                return _intParameters[parameterName];
            }
            set
            {
                if (isUploaded())
                {
                    TextureObject previousTexture = GLState.getTexture(target);
                    bind();
                    GL.TexParameter(target, parameterName, value);
                    GLState.bindTexture(target, previousTexture);
                }
                _intParameters[parameterName] = value;
            }
        }

        public TextureObject getTextureObject()
        {
            _upload();
            return _object;
        }
    }

    public class TexImage1D : TexImage
    {
        private int _width;
        public int width
        {
            get { return _width; }
            set
            {
                resize(value);
            }
        }

        public TexImage1D(TextureTarget target, PixelInternalFormat internalFormat, PixelFormat format, PixelType type, int width)
            : base(target, internalFormat, format, type)
        {
            this._width = width;
        }

        public TexImage1D(TexDataProvider dataProvider, TextureTarget target, PixelInternalFormat internalFormat, PixelFormat format, PixelType type, int width)
            : base(dataProvider, target, internalFormat, format, type)
        {
            this._width = width;
        }

        protected override void _uploadData()
        {
            if (dataProvider != null)
            {
                GL.TexImage1D(target, 0, internalFormat, _width, 0, format, type, dataProvider.getData(this));
            }
            else
            {
                GL.TexImage1D(target, 0, internalFormat, _width, 0, format, type, (IntPtr)0);
            }
        }

        public void resize(int width)
        {
            _width = width;
            _onResize();
        }
    }

    public class TexImage2D : TexImage
    {
        private int _width;
        public int width
        {
            get { return _width; }
            set
            {
                resize(value, _height);
            }
        }

    private int _height;
        public int height
        {
            get { return _height; }
            set
            {
                resize(_width, value);
            }
        }

        public TexImage2D(TextureTarget target, PixelInternalFormat internalFormat, PixelFormat format, PixelType type, int width, int height)
            : base(target, internalFormat, format, type)
        {
            this._width = width;
        this._height = height;
        }

        public TexImage2D(TexDataProvider dataProvider, TextureTarget target, PixelInternalFormat internalFormat, PixelFormat format, PixelType type, int width, int height)
            : base(dataProvider, target, internalFormat, format, type)
        {
            this._width = width;
            this._height = height;
        }

        protected override void _uploadData()
        {
            if (dataProvider != null)
            {
                GL.TexImage2D(target, 0, internalFormat, _width, _height, 0, format, type, dataProvider.getData(this));
            }
            else
            {
                GL.TexImage2D(target, 0, internalFormat, _width, _height, 0, format, type, (IntPtr)0);
            }
        }

        public void resize(int width, int height)
        {
            _width = width;
            _height = height;
            _onResize();
        }
    }

    public class TexImage3D : TexImage
    {
        private int _width;
        public int width
        {
            get { return _width; }
            set
            {
                resize(value, _height, _depth);
            }
        }

        private int _height;
        public int height
        {
            get { return _height; }
            set
            {
                resize(_width, value, _depth);
            }
        }

        private int _depth;
        public int depth
        {
            get { return _depth; }
            set
            {
                resize(_width, _height, value);
            }
        }

        public TexImage3D(TextureTarget target, PixelInternalFormat internalFormat, PixelFormat format, PixelType type, int width, int height, int depth)
            : base(target, internalFormat, format, type)
        {
            this._width = width;
            this._height = height;
            this._depth = depth;
        }

        public TexImage3D(TexDataProvider dataProvider, TextureTarget target, PixelInternalFormat internalFormat, PixelFormat format, PixelType type, int width, int height, int depth)
            : base(dataProvider, target, internalFormat, format, type)
        {
            this._width = width;
            this._height = height;
            this._depth = depth;
        }

        protected override void _uploadData()
        {
            if (dataProvider != null)
            {
                GL.TexImage3D(target, 0, internalFormat, _width, _height, _depth, 0, format, type, dataProvider.getData(this));
            }
            else
            {
                GL.TexImage3D(target, 0, internalFormat, _width, _height, _depth, 0, format, type, (IntPtr)0);
            }
        }

        public void resize(int width, int height, int depth)
        {
            _width = width;
            _height = height;
            _depth = depth;
            _onResize();
        }
    }
}
