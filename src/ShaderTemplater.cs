﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CSGraphics
{
    /// <summary>
    /// An interface for loading of loading shader source strings through a shader name reference.
    /// </summary>
    public interface ShaderSourceLoader
    {
        /// <summary>
        /// Get the source for a shader by its name.
        /// </summary>
        /// <param name="shaderName">The name of the shader to load.</param>
        /// <returns></returns>
        String getShaderSource(String shaderName);

        /// <summary>
        /// Get the source for a shader by its name.
        /// </summary>
        /// <param name="shaderName">The name of the shader to load.</param>
        /// <param name="parentShaderName">The name of a parent shader which requests to load this shader.</param>
        /// <returns></returns>
        String getShaderSource(String shaderName, String parentShaderName);
    }

    public class ShaderTemplater
    {
        public class ShaderTemplaterException : Exception
        {
            public ShaderTemplaterException(String message, int lineNumber, String shaderName)
                : base(String.Format("{0} on line {1} in shader {2}", message, lineNumber, shaderName))
            { }
        }

        public static ShaderSourceLoader shaderSourceLoader;

        public static String parse(String shaderName, Dictionary<String, String> parameters)
        {
            if (shaderSourceLoader == null)
            {
                throw new NullReferenceException("No ShaderSourceLoader bound");
            }
            return parseSource(shaderName, shaderSourceLoader.getShaderSource(shaderName), parameters);
        }

        /// <summary>
        /// Takes a shader source string with a set of parameters and parses it. Occurences of "$(PARAM_NAME)" in the source string will be replaced by the value of the key "PARAM_NAME" in the parameters.
        /// Also supports the following conditional statements:
        /// $ifdef, $ifndef, $elifdef, $elifndef, $else, $endif
        /// </summary>
        /// <param name="shaderName">The name of the shader source which will be fetched through the ShaderSourceLoader</param>
        /// <param name="source">The shader source</param>
        /// <param name="parameters">The parameters to be used for parsing</param>
        /// <returns></returns>
        public static String parseSource(String shaderName, String source, Dictionary<String, String> parameters)
        {
            foreach (KeyValuePair<String, String> entry in parameters)
            {
                source = source.Replace("$(" + entry.Key + ")", entry.Value);
            }

            StringReader reader = new StringReader(source);
            StringWriter writer = new StringWriter();

            uint nestingLevel = 0;
            uint blockingNestingLevel = Constants.SHADER_MAX_NESTING;
            bool[] chainFulfilled = new bool[Constants.SHADER_MAX_NESTING + 1];

            int lineNumber = 1;
            for (String lineIn; (lineIn = reader.ReadLine()) != null; lineNumber++)
            {
                String trimmedLineIn = lineIn.Trim();

                if (trimmedLineIn.StartsWith("$", StringComparison.OrdinalIgnoreCase))
                {
                    String[] splitString = lineIn.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    String keyword = splitString.Length > 0 ? splitString[0] : null;
                    String paramName = splitString.Length > 1 ? splitString[1] : null;

                    if (keyword.Equals("$ifdef", StringComparison.OrdinalIgnoreCase))
                    {
                        if (paramName == null)
                        {
                            throw new ShaderTemplaterException("Missing parameter name in $ifdef statement", lineNumber, shaderName);
                        }

                        nestingLevel++;
                        if (blockingNestingLevel > nestingLevel)
                        {
                            if (!parameters.ContainsKey(paramName))
                            {
                                blockingNestingLevel = nestingLevel;
                            }
                            else
                            {
                                chainFulfilled[nestingLevel] = true;
                            }
                        }
                    }
                    else if (keyword.Equals("$ifndef", StringComparison.OrdinalIgnoreCase))
                    {
                        if (paramName == null)
                        {
                            throw new ShaderTemplaterException("Missing parameter name in $ifndef statement", lineNumber, shaderName);
                        }

                        nestingLevel++;
                        if (blockingNestingLevel > nestingLevel)
                        {
                            if (parameters.ContainsKey(paramName))
                            {
                                blockingNestingLevel = nestingLevel;
                            }
                            else
                            {
                                chainFulfilled[nestingLevel] = true;
                            }
                        }
                    }
                    else if (keyword.Equals("$elifdef", StringComparison.OrdinalIgnoreCase))
                    {
                        if (paramName == null)
                        {
                            throw new ShaderTemplaterException("Missing parameter name in $elifdef statement", lineNumber, shaderName);
                        }

                        if (!chainFulfilled[nestingLevel] && blockingNestingLevel > nestingLevel)
                        {
                            if (parameters.ContainsKey(paramName))
                            {
                                blockingNestingLevel = Constants.SHADER_MAX_NESTING;
                                chainFulfilled[nestingLevel] = true;
                            }
                            else
                            {
                                blockingNestingLevel = nestingLevel;
                            }
                        }
                    }
                    else if (keyword.Equals("$elifndef", StringComparison.OrdinalIgnoreCase))
                    {
                        if (paramName == null)
                        {
                            throw new ShaderTemplaterException("Missing parameter name in $elifndef statement", lineNumber, shaderName);
                        }

                        if (!chainFulfilled[nestingLevel] && blockingNestingLevel > nestingLevel)
                        {
                            if (!parameters.ContainsKey(paramName))
                            {
                                blockingNestingLevel = Constants.SHADER_MAX_NESTING;
                                chainFulfilled[nestingLevel] = true;
                            }
                            else
                            {
                                blockingNestingLevel = nestingLevel;
                            }
                        }
                    }
                    else if (keyword.Equals("$else", StringComparison.OrdinalIgnoreCase))
                    {
                        if (paramName != null)
                        {
                            throw new ShaderTemplaterException("Unexpected parameter name in $else statement", lineNumber, shaderName);
                        }

                        if (blockingNestingLevel > nestingLevel)
                        {
                            if (!chainFulfilled[nestingLevel])
                            {
                                blockingNestingLevel = Constants.SHADER_MAX_NESTING;
                                chainFulfilled[nestingLevel] = true;
                            }
                            else
                            {
                                blockingNestingLevel = nestingLevel;
                            }
                        }
                    }
                    else if (keyword.Equals("$endif", StringComparison.OrdinalIgnoreCase))
                    {
                        if (paramName != null)
                        {
                            throw new ShaderTemplaterException("Unexpected parameter name in $endif statement", lineNumber, shaderName);
                        }

                        nestingLevel--;
                        if (blockingNestingLevel > nestingLevel)
                        {
                            blockingNestingLevel = Constants.SHADER_MAX_NESTING;
                            chainFulfilled[nestingLevel + 1] = false;
                        }
                    }

                    writer.WriteLine("//" + lineIn);
                }
                else
                {
                    writer.WriteLine(lineIn);
                }
            }

            return writer.ToString();
        }
    }
}
